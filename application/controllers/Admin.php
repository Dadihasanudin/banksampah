<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	var $API ="";

	public function __construct()
	{
		parent::__construct();
		$this->API="http://localhost/serverbanksampah/index.php";
		$this->load->library('curl');
		$this->load->model('model_admin');
		if($this->session->userdata('status_login') != "login"){
			redirect(base_url("login/index_admin"));
		  }
	}

	public function getProfil()
	{
		$test = $this->session->userdata('id');
        // print_r($test); die;
		$data['sektor'] = $this->model_admin->getSektor();
		
		$this->load->view('components_admin/header');
		$this->load->view('components_admin/sidebar');
		$this->load->view('admin/profil',$data);
		$this->load->view('components_admin/footer');
	}

	public function updateProfil()
	{
		$this->model_admin->updateProfil();
		echo"<script>alert('Profil berhasil diperbarui! Lakukan login kembali'); window.location = '../login/index_admin'</script>";
	}

	public function changeFotoProfil()
	{
		$config['upload_path'] = './assets/user/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '2000';

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('foto')) {
			$error = $this->upload->display_errors();
			// menampilkan pesan error
			print_r($error);
		} else {
			$result = $this->upload->data();
			$name = $result['file_name'];
			$this->model_admin->changeFotoProfil($name);
			echo"<script>alert('Profil berhasil diperbarui! Lakukan login kembali'); window.location = '../login/index_admin'</script>";
		}
	}

//---------------------------------------------------------------------------------------------------------------------//	

	public function getPegawai()
	{
		$data['sektor'] = $this->model_admin->getSektor();
		$data['sektor2'] = $this->model_admin->getSektor();
		$data['pegawai'] = $this->model_admin->getPegawai();

		$this->load->view('components_admin/header');
		$this->load->view('components_admin/sidebar');
		$this->load->view('admin/pegawai',$data);
		$this->load->view('components_admin/footer');
	}

	public function insertPegawai()
	{
		$config['upload_path'] = './assets/user/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '2000';

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('foto')) {
			$error = $this->upload->display_errors();
			// menampilkan pesan error
			print_r($error);
		} else {
			$result = $this->upload->data();
			$name = $result['file_name'];
		$this->model_admin->insertPegawai($name);
		redirect('admin/getPegawai');
		}
	}
	
	public function tambahPegawai()
	{
		$data['sektor'] = $this->model_admin->getSektor();
		$data['sektor2'] = $this->model_admin->getSektor();
		$data['pegawai'] = $this->model_admin->getPegawai();

		$this->load->view('components_admin/header');
		$this->load->view('components_admin/sidebar');
		$this->load->view('admin/pegawai_tambah',$data);
		$this->load->view('components_admin/footer');
	}

	public function editPegawai()
	{
		
		$where = array('id' => $id);
		$data['petugas'] = $this->model_admin->EditAllData($where, 'tb_user')->result();

		$this->load->view('components_admin/header');
		$this->load->view('components_admin/sidebar');
		$this->load->view('admin/pegawai_edit',$data);
		$this->load->view('components_admin/footer');
	}

	public function showPegawai($id)
	{
		$q = $this->db->get_where('tb_user',array('id' => $id));
        return $q->row_array();
	}

	public function updatePegawai()
	{
		$id = $this->input->post('id_edit');

		$data = array(
			'nama_lengkap' => $this->input->post('nama_lengkap_edit'),
			'username' => $this->input->post('username_edit'),
			'no_telp' => $this->input->post('no_telp_edit'),
			'alamat' => $this->input->post('alamat_edit'),
			'jabatan' => $this->input->post('jabatan_edit'),
			'status' => $this->input->post('status_edit'),
			'id_sektor' => $this->input->post('sektor_edit')
		);

		$this->model_admin->updatePegawai($id,$data);

		redirect('admin/getPegawai');
	}
	
	public function deletePegawai($id)
	{
		$this->model_admin->deletePegawai($id);
		redirect('admin/getPegawai');
	}

	//----------------------------------------------------------------------------------------------------------------//

	public function getBarang()
	{
		$data['barang'] = $this->model_admin->getBarang();
		$data['kategori'] = $this->model_admin->getKategori();
		$data['kategori_edit'] = $this->model_admin->getKategori();

		$this->load->view('components_admin/header');
		$this->load->view('components_admin/sidebar');
		$this->load->view('admin/barang',$data);
		$this->load->view('components_admin/footer');
	}

	public function insertBarang()
	{
		$config['upload_path'] = './assets/barang/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '2000';

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('foto')) {
			$error = $this->upload->display_errors();
			// menampilkan pesan error
			print_r($error);
		} else {
			$result = $this->upload->data();
			$name = $result['file_name'];
			$this->model_admin->insertBarang($name);
			redirect('admin/getBarang');
		}
	}

	public function showBarang($id)
	{
		$q = $this->db->get_where('tb_barang',array('id' => $id))->result_array();
		$q = json_encode($q);
		print_r($q); die;
        return $q;
	}

	public function updateBarang()
	{
		$id = $this->input->post('id_edit');

		$config['upload_path'] = './assets/barang/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '2000';

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('input_foto')) {
			$this->model_admin->updateBarang($id);

			redirect('admin/getBarang');
		} else {
			$result = $this->upload->data();
			$name = $result['file_name'];
			$this->model_admin->updateBarangG($id,$name);
			redirect('admin/getBarang');
		}
		
	}

	public function deleteBarang($id)
	{
		$this->model_admin->deleteBarang($id);
		redirect('admin/getBarang');
	}

	//------------------------------------------------------------------------------------------------------------------//

	public function getSektor()
	{
		$data['sektor'] = json_decode($this->curl->simple_get($this->API.'/sektor'));

		$this->load->view('components_admin/header');
		$this->load->view('components_admin/sidebar');
		$this->load->view('admin/sektor',$data);
		$this->load->view('components_admin/footer');
	}

	public function insertSektor()
	{
		$data = array(
            'sektor' => $this->input->post('sektor')
		);
		$insert =  $this->curl->simple_post($this->API.'/sektor', $data, array(CURLOPT_BUFFERSIZE => 10)); 
		// $this->model_admin->insertsektor();

		redirect('admin/getsektor');
	}

	public function showSektor($id)
	{
		$q = $this->db->get_where('tb_sektor',array('id' => $id))->result_array();
		$q = json_encode($q);
		print_r($q); die;
        return $q;
	}

	public function updateSektor()
	{
		$data = array(
			'id' => $this->input->post('id_edit'),
            'sektor' => $this->input->post('nama_sektor_edit')
		);
		

		$update =  $this->curl->simple_put($this->API.'/sektor', $data, array(CURLOPT_BUFFERSIZE => 10)); 
		// print_r($update); die;
		// $this->model_admin->updatesektor($id);

		redirect('admin/getsektor');
	}

	public function deleteSektor($id)
	{
		$this->curl->simple_delete($this->API.'/sektor', array('id'=>$id), array(CURLOPT_BUFFERSIZE => 10)); 
		// $this->model_admin->deletesektor($id);

		redirect('admin/getsektor');
	}

	//--------------------------------------------------------------------------------------------------------------//


	public function getJenisSampah()
	{
		$data['jenis'] = $this->model_admin->getJenisSampah();

		$this->load->view('components_admin/header');
		$this->load->view('components_admin/sidebar');
		$this->load->view('admin/jenis_sampah',$data);
		$this->load->view('components_admin/footer');
	}

	public function tambahSampah()
	{

		$this->load->view('components_admin/header');
		$this->load->view('components_admin/sidebar');
		$this->load->view('admin/jenis_sampah_tambah');
		$this->load->view('components_admin/footer');
	}

	public function editJenisSampah($id)
	{
		$where = array('id' => $id);
		$data['jenis'] = $this->model_admin->EditAllData($where, 'tb_jenis_sampah')->result();

		$this->load->view('components_admin/header');
		$this->load->view('components_admin/sidebar');
		$this->load->view('admin/jenis_sampah_edit',$data);
		$this->load->view('components_admin/footer');
	}

	public function insertJenisSampah()
	{
		$this->model_admin->insertJenisSampah();

		redirect('admin/getJenisSampah');
	}

	public function showJenisSampah($id)
	{
		$q = $this->db->get_where('tb_jenis_sampah',array('id' => $id))->result_array();
		$q = json_encode($q);
		print_r($q); die;
        return $q;
	}

	public function updateJenisSampah()
	{
		$id = $this->input->post('id_edit');

		$this->model_admin->updateJenisSampah($id);

		redirect('admin/getJenisSampah');
	}

	public function deleteJenisSampah($id)
	{
		$this->model_admin->deleteJenisSampah($id);

		redirect('admin/getJenisSampah');
	}

	//--------------------------------------------------------------------------------------------------------------//

	public function getRequest()
	{
		$data['request'] = $this->model_admin->getRequest();
		$data['pegawai'] = $this->model_admin->getPegawaiKosong();

		$this->load->view('components_admin/header');
		$this->load->view('components_admin/sidebar');
		$this->load->view('admin/request',$data);
		$this->load->view('components_admin/footer');
	}

	public function getPetugasSektor()
	{
		$id_nasabah = $this->input->post('id_nasabah');
		// print_r($id_nasabah); die;
		$sektor = $this->model_admin->getNasabahSektor($id_nasabah);
		$sektorFix = $sektor->id_sektor;

		$wakwaw = $this->db->query("
		select * from tb_user where jabatan = 'petugas' and id_sektor = $sektorFix")->result_array();
		$new = json_encode($wakwaw);
		echo $new;
	}

	public function showRequest($id)
	{
		$q = $this->db->query('SELECT tb_request.id as id, tb_nasabah.id as id_nasabah, tb_nasabah.nama_lengkap as nama_nasabah, tb_nasabah.alamat as alamat_nasabah, tb_request.status as status , tb_user.id as id_user, tb_user.nama_lengkap as nama_petugas FROM `tb_request` left join tb_user on tb_user.id = tb_request.id_pegawai left join tb_nasabah on tb_nasabah.id = tb_request.id_nasabah where tb_request.id = '.$id)->result_array();
		$q = json_encode($q);
		print_r($q); die;
        return $q;
	}

	public function updateRequest()
	{
		$id = $this->input->post('id_edit');

		$this->model_admin->updateRequest($id);

		redirect('admin/getRequest');
	}

	public function deleteRequest($id)
	{
		$this->model_admin->deleteRequest($id);

		redirect('admin/getRequest');
	}

	//--------------------------------------------------------------------------------------------------------------//

	public function getTransaksiSampah()
	{
		$data['transaksi'] = $this->model_admin->getTransaksiSampah();

		$this->load->view('components_admin/header');
		$this->load->view('components_admin/sidebar');
		$this->load->view('admin/transaksi_s',$data);
		$this->load->view('components_admin/footer');
	}

	public function showTransaksiSampah($id)
	{
		$q = $this->db->query('SELECT tb_transaksi_sampah_detail.berat, tb_transaksi_sampah_detail.harga, tb_jenis_sampah.jenis_sampah FROM tb_transaksi_sampah_detail inner join tb_jenis_sampah on tb_jenis_sampah.id = tb_transaksi_sampah_detail.id_jenis_sampah where id_transaksi_sampah ='.$id)->result_array();
		$q = json_encode($q);
		print_r($q); die;
        return $q;
	}

	public function deleteTransaksiSampah($id)
	{
		$this->model_admin->deleteTransaksiSampah($id);
		$this->model_admin->deleteTransaksiSampah2($id);

		redirect('admin/getTransaksiSampah');
	}
	//--------------------------------------------------------------------------------------------------------------//

	public function getTransaksiBarang()
	{
		$data['transaksi'] = $this->model_admin->getTransaksiBarang();

		$this->load->view('components_admin/header');
		$this->load->view('components_admin/sidebar');
		$this->load->view('admin/transaksi_b',$data);
		$this->load->view('components_admin/footer');
	}

	public function showTransaksiBarang($id)
	{
		$q = $this->db->query('SELECT tb_transaksi_barang_detail.jumlah, tb_transaksi_barang_detail.total_harga, tb_barang.nama_barang FROM tb_transaksi_barang_detail inner join tb_barang on tb_barang.id = tb_transaksi_barang_detail.id_barang where id_transaksi_barang ='.$id)->result_array();
		$q = json_encode($q);
		print_r($q); die;
        return $q;
	}

	public function deleteTransaksiBarang($id)
	{
		$this->model_admin->deleteTransaksiBarang($id);
		$this->model_admin->deleteTransaksiBarang2($id);

		redirect('admin/getTransaksiBarang');
	}

	//---------------------------------------------------------------------------------------------------------------------//	

	public function getNasabah()
	{
		$data['nasabah'] = $this->model_admin->getNasabah();
		$data['sektor'] = $this->model_admin->getSektor();

		$this->load->view('components_admin/header');
		$this->load->view('components_admin/sidebar');
		$this->load->view('admin/nasabah',$data);
		$this->load->view('components_admin/footer');
	}

	public function insertNasabah()
	{
		$this->model_admin->insertNasabah();
		redirect('admin/getNasabah');
	}
	
	public function showNasabah($id)
	{
		$q = $this->db->get_where('tb_nasabah',array('id' => $id))->result_array();
		$q = json_encode($q);
		print_r($q); die;
        return $q;
	}

	public function updateNasabah()
	{
		$id = $this->input->post('id_edit');

		$data = array(
			'status' => $this->input->post('status_edit')
		);

		$this->model_admin->updateNasabah($id,$data);

		redirect('admin/getNasabah');
	}
	
	public function deleteNasabah($id)
	{
		$this->model_admin->deleteNasabah($id);
		redirect('admin/getNasabah');
	}

	public function tukarUang()
	{
		$id = $this->input->post('id_edit');
		// print_r($id); die;
		$this->model_admin->tukarUang($id);
		redirect('admin/getNasabah');
	}

}
