<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper("url_helper");
		$this->load->model('model_login');
	}

	public function index_nasabah()
	{
		$this->load->view('login/login_nasabah');
	}

	public function index_admin()
	{
		$this->load->view("login/login_admin");
	}

	public function proses_admin()
	{
		$data = array(
		'username' => $this->input->post('username'),
		'password' => md5($this->input->post('password')),
		'status'   => 'aktif'
		);
		
		$hasil = $this->model_login->cek_login($data);
		if ($hasil->num_rows() == 1) {
			foreach ($hasil->result() as $sess) {
				$sess_data['id'] = $sess->id;
        		$sess_data['nama_lengkap'] = $sess->nama_lengkap;
				$sess_data['username'] = $sess->username;
				$sess_data['password'] = $sess->password;
				$sess_data['alamat'] = $sess->alamat;
				$sess_data['no_telp'] = $sess->no_telp;
				$sess_data['jabatan'] = $sess->jabatan;
				$sess_data['foto'] = $sess->foto;
				$sess_data['status'] = $sess->status;
				$sess_data['status_login'] = 'login';
				$sess_data['pw'] = $this->input->post('password');
				$this->session->set_userdata($sess_data);
			}
			
			if ($sess_data['jabatan'] == 'admin'){
				redirect('admin/getProfil');
			}else{
				redirect('petugas/getProfil');
			}
    }
      else
      echo"<script>alert('Username atau Password anda salah!'); window.location = '../login/index_admin'</script>";
	}

	public function proses_nasabah()
	{
		$data = array(
		'username' => $this->input->post('username'),
		'password' => md5($this->input->post('password')),
		'status'   => 'aktif'
		);

		$hasil = $this->model_login->cek_login_nasabah($data);
		if ($hasil->num_rows() == 1) {
			foreach ($hasil->result() as $sess) {
				$sess_data['id'] = $sess->id;
        		$sess_data['nama_lengkap'] = $sess->nama_lengkap;
				$sess_data['no_rekening'] = $sess->no_rekening;
				$sess_data['username'] = $sess->username;
				$sess_data['password'] = $sess->password;
				$sess_data['alamat'] = $sess->alamat;
				$sess_data['no_telp'] = $sess->no_telp;
				$sess_data['status'] = $sess->status;
				$sess_data['status_login'] = 'login';
				$sess_data['pw'] = $this->input->post('password');
				$this->session->set_userdata($sess_data);
			}
				redirect('nasabah/getProfil');
    }
      else
      echo"<script>alert('Username atau Password anda salah!'); window.location = '../login/index_admin'</script>";
	}

	public function logout_admin()
	{
		$this->session->sess_destroy();
		redirect('login/index_admin');
	}

}
