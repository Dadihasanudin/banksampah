<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Petugas extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_petugas');
		$this->load->library('cart');
		if($this->session->userdata('status_login') != "login"){
			redirect(base_url("login/index_admin"));
		  }
	}

	public function getProfil()
	{
		$data['sektor'] = $this->model_petugas->getSektor();

		$this->load->view('components_petugas/header');
		$this->load->view('components_petugas/sidebar');
		$this->load->view('petugas/profil',$data);
		$this->load->view('components_petugas/footer');
	}

	public function updateProfil()
	{
		$this->model_petugas->updateProfil();
		echo"<script>alert('Profil berhasil diperbarui! Lakukan login kembali'); window.location = '../login/index_admin'</script>";
	}

	public function changeFotoProfil()
	{
		$config['upload_path'] = './assets/user/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '2000';

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('foto')) {
			$error = $this->upload->display_errors();
			// menampilkan pesan error
			print_r($error);
		} else {
			$result = $this->upload->data();
			$name = $result['file_name'];
			$this->model_petugas->changeFotoProfil($name);
			echo"<script>alert('Profil berhasil diperbarui! Lakukan login kembali'); window.location = '../login/index_admin'</script>";
		}
	}

//---------------------------------------------------------------------------------------------------------------------//	

	public function getRequest()
	{
		$data['request'] = $this->model_petugas->getRequest();
		$data['jenis_sampah'] = $this->model_petugas->getJenisSampah();
		$data['saldo'] = $this->model_petugas->getNasabah();

		$this->load->view('components_petugas/header');
		$this->load->view('components_petugas/sidebar');
		$this->load->view('petugas/request',$data);
		$this->load->view('components_petugas/footer');
	}

	public function insertRequest()
	{
		$this->model_petugas->insertRequest();
		redirect('petugas/getRequest');

	}

	public function getHargaSampah()
	{
		$id_jenis_sampah = $this->input->post('id_jenis_sampah');
		$this->model_petugas->getHargaSampah($id_jenis_sampah);
	}

	public function updateStatusRequest()
	{
		$id = $this->input->post('id');
		$this->model_petugas->updateStatusRequest($id);

		redirect('petugas/getRequest');
	}

	public function cartSampah()
	{
		$berat = $this->input->post('berat');
		$harga = $this->input->post('harga');
		$total = $berat * $harga;
		// print_r($total); die;

		$data = array(
			'id'      				=> $this->input->post('id_jenis_sampah'),
			'name'      			=> $this->input->post('jenis_sampah'),
			'qty'     				=> $this->input->post('berat'),
			'satuan'     			=> $this->input->post('satuan'),
			'price'     			=> $this->input->post('harga'),
			'total'     			=> $total
		);

			$this->cart->insert($data);
			redirect('petugas/getRequest');
		}
		
		public function deleteCart($rowid)
		{
			$data = array(
				'rowid' => $rowid,
				'qty' => 0
			);
			
			$this->cart->update($data);
			redirect('petugas/getRequest');
	}

	public function toCheckout($id)
	{
		$this->load->view('components_petugas/header');
		$this->load->view('components_petugas/sidebar');
		$this->load->view('petugas/checkout');
		$this->load->view('components_petugas/footer');
	}

	public function insertCheckout()
	{
		$id_request = $this->input->post('id_request');
		
		$id_nasabah = $this->model_petugas->getIdNasabah($id_request);
		$id_nasabahFix = $id_nasabah->id_nasabah;
		
		$this->model_petugas->insertToTransaksi($id_nasabahFix);
		$this->model_petugas->updateSaldo($id_nasabahFix);
		$this->model_petugas->updateStatusRequestSelesai($id_request);

		
		foreach ($this->cart->contents() as $key) {
			
			$q = $this->db->query("SELECT max(id_transaksi_sampah) as id_transaksi from tb_transaksi_sampah")->row();
			$id_transaksi = $q->id_transaksi;

			$data=array(
                'id_transaksi_sampah' => $id_transaksi,
                'id_jenis_sampah' => $key['id'],
                'berat' => $key['qty'],
                'harga' => $key['price'],
            );
			$this->model_petugas->insertToTransaksiDetail($data);
		}
		$this->cart->destroy();
		redirect('petugas/getRequest');		
	}

	//--------------------------------------------------------------------------------------------------------------//

	public function Transaksi()
	{
		$this->load->view('components_petugas/header');
		$this->load->view('components_petugas/sidebar');
		$this->load->view('petugas/transaksi');
		$this->load->view('components_petugas/footer');
	}

	public function Tarik()
	{
		$this->load->view('components_petugas/header');
		$this->load->view('components_petugas/sidebar');
		$this->load->view('petugas/tarik');
		$this->load->view('components_petugas/footer');
	}

	public function Tabung()
	{
		$this->load->view('components_petugas/header');
		$this->load->view('components_petugas/sidebar');
		$this->load->view('petugas/tabung');
		$this->load->view('components_petugas/footer');
	}

	//--------------------------------------------------------------------------------------------------------------//

	public function getTransaksiSampah()
	{
		$data['transaksi'] = $this->model_petugas->getTransaksiSampah();

		$this->load->view('components_petugas/header');
		$this->load->view('components_petugas/sidebar');
		$this->load->view('petugas/transaksi_s',$data);
		$this->load->view('components_petugas/footer');
	}

	public function showTransaksiSampah($id)
	{
		$q = $this->db->query('SELECT tb_transaksi_sampah_detail.berat, tb_transaksi_sampah_detail.harga, tb_jenis_sampah.jenis_sampah FROM tb_transaksi_sampah_detail inner join tb_jenis_sampah on tb_jenis_sampah.id = tb_transaksi_sampah_detail.id_jenis_sampah where id_transaksi_sampah ='.$id)->result_array();
		$q = json_encode($q);
		print_r($q); die;
        return $q;
	}

	public function deleteTransaksiSampah($id)
	{
		$this->model_petugas->deleteTransaksiSampah($id);
		$this->model_petugas->deleteTransaksiSampah2($id);

		redirect('petugas/getTransaksiSampah');
	}
	//--------------------------------------------------------------------------------------------------------------//

	public function getTransaksiBarang()
	{
		$data['transaksi'] = $this->model_petugas->getTransaksiBarang();

		$this->load->view('components_petugas/header');
		$this->load->view('components_petugas/sidebar');
		$this->load->view('petugas/transaksi_b',$data);
		$this->load->view('components_petugas/footer');
	}

	public function showTransaksiBarang($id)
	{
		$q = $this->db->query('SELECT tb_transaksi_barang_detail.jumlah, tb_transaksi_barang_detail.total_harga, tb_barang.nama_barang FROM tb_transaksi_barang_detail inner join tb_barang on tb_barang.id = tb_transaksi_barang_detail.id_barang where id_transaksi_barang ='.$id)->result_array();
		$q = json_encode($q);
		print_r($q); die;
        return $q;
	}

	public function deleteTransaksiBarang($id)
	{
		$this->model_petugas->deleteTransaksiBarang($id);
		$this->model_petugas->deleteTransaksiBarang2($id);

		redirect('petugas/getTransaksiBarang');
	}

	//---------------------------------------------------------------------------------------------------------------------//	

	public function getNasabah()
	{
		$data['nasabah'] = $this->model_petugas->getNasabah();
		$data['sektor'] = $this->model_petugas->getSektor();

		$this->load->view('components_petugas/header');
		$this->load->view('components_petugas/sidebar');
		$this->load->view('petugas/nasabah',$data);
		$this->load->view('components_petugas/footer');
	}

	public function insertNasabah()
	{
		$this->model_petugas->insertNasabah();
		$this->load->view('petugas/test');
		// redirect('petugas/test');

	}
	
	public function showNasabah($id)
	{
		$q = $this->db->get_where('tb_nasabah',array('id' => $id))->result_array();
		$q = json_encode($q);
		print_r($q); die;
        return $q;
	}

	public function showNasabahreq($id)
	{
		$q = $this->db->get_where('tb_request',array('id' => $id))->result_array();
		$q = json_encode($q);
		print_r($q); die;
        return $q;
	}

	public function updateNasabah()
	{
		$id = $this->input->post('id_edit');

		$data = array(
			'status' => $this->input->post('status_edit')
		);

		$this->model_petugas->updateNasabah($id,$data);

		redirect('petugas/getNasabah');
	}
	
	public function deleteNasabah($id)
	{
		$this->model_petugas->deleteNasabah($id);
		redirect('petugas/getNasabah');
	}

	public function tukarUang()
	{
		$id = $this->input->post('id_edit');
		// print_r($id); die;
		$this->model_petugas->tukarUang($id);
		redirect('petugas/getNasabah');
	}

	public function test()
	{
		$this->load->view('petugas/test');
		// redirect('petugas/getNasabah');
	}

}
