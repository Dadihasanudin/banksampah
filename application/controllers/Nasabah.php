<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nasabah extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_nasabah');
		$this->load->library('cart');
		if($this->session->userdata('status_login') != "login"){
			redirect(base_url("login/index_nasabah"));
		  }
	}

	public function getProfil()
	{
        $test = $this->session->userdata('id');
        // print_r($test); die;
		$data['sektor'] = $this->model_nasabah->getSektor();

		$this->load->view('components_nasabah/header');
		$this->load->view('components_nasabah/sidebar');
		$this->load->view('nasabah/profil',$data);
		$this->load->view('components_nasabah/footer');
	}

	public function updateProfil()
	{
		$this->model_nasabah->updateProfil();
		echo"<script>alert('Profil berhasil diperbarui! Lakukan login kembali'); window.location = '../login/index_admin'</script>";
	}
	
	//--------------------------------------------------------------------------------------------------------------//

	public function getTransaksiSampah()
	{
		$id = $this->session->userdata('id');
		// print_r($id); die;
		$data['transaksi'] = $this->model_nasabah->getDatabyid();

		$this->load->view('components_nasabah/header');
		$this->load->view('components_nasabah/sidebar');
		$this->load->view('nasabah/transaksi_s',$data);
		$this->load->view('components_nasabah/footer');
	}

	public function showTransaksiSampah($id)
	{
		$q = $this->db->query('SELECT tb_transaksi_sampah_detail.berat, tb_transaksi_sampah_detail.harga, tb_jenis_sampah.jenis_sampah FROM tb_transaksi_sampah_detail inner join tb_jenis_sampah on tb_jenis_sampah.id = tb_transaksi_sampah_detail.id_jenis_sampah where id_transaksi_sampah ='.$id)->result_array();
		$q = json_encode($q);
		print_r($q); die;
        return $q;
	}
    
}    