<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'welcome';
$route['own'] = 'login/index_admin';
$route['use'] = 'login/index_nasabah';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
