<?php
  class Model_petugas extends ci_model{

    public function __construct()
    {
        parent::__construct();
    }

    public function getSektor()
    {
        return $this->db->get('tb_sektor')->result_array();
    }

    public function updateProfil()
    {
        $id = $this->input->post('id_user');

        $data = array(
            'nama_lengkap' => $this->input->post('nama_lengkap'),
            'username' => $this->input->post('username'),
            'password' => md5($this->input->post('password')),
            'no_telp' => $this->input->post('no_telp'),
            'alamat' => $this->input->post('alamat'),
            'id_sektor' => $this->input->post('id_sektor'),
        );

        $this->db->where('id',$id);
        return $this->db->update('tb_user',$data);
    }

    public function changeFotoProfil($name)
    {
        $id = $this->input->post('id_user');

        $data = array(
            'foto' => $name
        );

        $this->db->where('id',$id);
        return $this->db->update('tb_user',$data);
    }

    //-----------------------------------------------------------------------------------------------------------------//

    public function getRequest()
    {
        $id_pegawai = $this->session->userdata('id');
        $q = $this->db->query('SELECT tb_request.id as id, tb_nasabah.id as id_nasabah, tb_nasabah.nama_lengkap as nama_nasabah, tb_nasabah.alamat as alamat_nasabah, tb_request.status as status , tb_user.id as id_user, tb_user.nama_lengkap as nama_petugas, tb_request.jenis_request as jenis_request FROM `tb_request` left join tb_user on tb_user.id = tb_request.id_pegawai left join tb_nasabah on tb_nasabah.id = tb_request.id_nasabah where tb_request.id_pegawai ='.$id_pegawai.' and tb_request.status != "selesai"');
        return $q->result_array();
    }

    public function insertRequest()
    {
        $norek=$this->input->post('no_rekening');

        $id=$this->db->get_where('tb_nasabah',array('no_rekening'=>$norek))->row();
        $idfix=$id->id;
        $data = array(
            'id_nasabah' =>$idfix,
            'id_pegawai' => $this->session->userdata('id'),
            'jenis_request' => $this->input->post('jenis_request'),
            'status' => 'pending'
        );
        // print_r($data); die;
        return $this->db->insert('tb_request',$data);

    }

    public function updateStatusRequest($id)
    {
        $data = array(
            'status' => 'menuju'
        );

        $this->db->where('id',$id);
        return $this->db->update('tb_request',$data);
    }
    
    public function getIdNasabah($id_request)
    {
        $this->db->where('id',$id_request);
        $this->db->select('id_nasabah');
        $q = $this->db->get('tb_request');
        return $q->row();
    }
    
    public function insertToTransaksi($id_nasabahFix)
    {
        $tgl=date('Y-m-d');
        $data = array(
            'id_nasabah'    => $id_nasabahFix,
            'tanggal'       => $tgl,
            'total_harga'   => $this->input->post('total')
        );

        return $this->db->insert('tb_transaksi_sampah',$data);
    }
    
    public function updateStatusRequestSelesai($id_request)
    {
        $data = array(
            'status' => 'selesai'
        );

        $this->db->where('id',$id_request);
        return $this->db->update('tb_request',$data);
    }

    public function insertToTransaksiDetail($data)
    {
        return $this->db->insert('tb_transaksi_sampah_detail',$data);
        
    }

    public function updateSaldo($id_nasabahFix)
    {
        $q = $this->db->get_where('tb_nasabah',array('id' => $id_nasabahFix))->row();
        $saldoAwal = $q->saldo;
        $saldoBaru = $this->input->post('total');
        $saldoFinal = $saldoAwal + $saldoBaru; 

        $data = array(
            'saldo' => $saldoFinal
        );

        $this->db->where('id', $id_nasabahFix);
        return $this->db->update('tb_nasabah', $data);
    }

    public function getJenisSampah()
    {
        $q = $this->db->get('tb_jenis_sampah');
        return $q->result_array();
    }
    
    public function getHargaSampah($id_jenis_sampah)
    {
        $q = $this->db->query("
        select * from tb_jenis_sampah where id = $id_jenis_sampah")->result_array();
        $new = json_encode($q);
        echo $new;
    }

    //---------------------------------------------------------------------------------------------------------------//

    public function getNasabah()
    {
        $this->db->join('tb_sektor','tb_sektor.id = tb_nasabah.id_sektor');
        $this->db->select('*,tb_sektor.id as id_sektor, tb_nasabah.id as id_nasabah');
        $q = $this->db->get('tb_nasabah');
        return $q->result_array();
    }

    public function insertNasabah()
    {
        $random1 = mt_rand(000,999);
        $random2 = mt_rand(000,999);
        $random3 = mt_rand(000,999);
        $random4 = mt_rand(000,999);
        $rek = "$random1$random2$random3$random4";
        // print_r($rek); die;
        $data = array(
            'nama_lengkap' => $this->input->post('nama_lengkap'),
            'no_rekening' => $rek,
            'username' => $this->input->post('username'),
            'password' => md5($this->input->post('username')),
            'no_telp' => $this->input->post('no_telp'),
            'alamat' => $this->input->post('alamat'),
            'id_sektor' => $this->input->post('id_sektor'),
            'saldo' => '',
            'status' => 'aktif'
        );

        $this->db->insert('tb_nasabah', $data);
    }

    public function updateNasabah($id,$data)
    {
        $this->db->where('id',$id);
        return $this->db->update('tb_nasabah',$data);
    }
    
    public function tukarUang($id)
    {
        $saldoAwal = $this->input->post('saldo_edit');
        $nominal = $this->input->post('nominal_edit');
        $saldoAkhir = $saldoAwal - $nominal;
        // print_r($saldoAkhir); die;
        $data = array(
            'saldo' => $saldoAkhir
        );
        $this->db->where('id',$id);
        return $this->db->update('tb_nasabah',$data);
    }

    public function deleteNasabah($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('tb_nasabah');
    }

    //-----------------------------------------------------------------------------------------------------------------//

    public function getTransaksiSampah()
    {
        $this->db->join('tb_nasabah','tb_nasabah.id = tb_transaksi_sampah.id_nasabah');
        $q = $this->db->get('tb_transaksi_sampah');
        return $q->result_array();
    }

    public function deleteTransaksiSampah($id)
    {
        $this->db->where('id_transaksi_sampah',$id);
        $this->db->delete('tb_transaksi_sampah');
    }
    public function deleteTransaksiSampah2($id)
    {
        $this->db->where('id_transaksi_sampah',$id);
        $this->db->delete('tb_transaksi_sampah_detail');
    }
   

}
