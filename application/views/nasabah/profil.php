<div class="main-panel">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar bar1"></span>
                    <span class="icon-bar bar2"></span>
                    <span class="icon-bar bar3"></span>
                </button>
                <a class="navbar-brand">Profil</a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="<?=base_url();?>index.php/login/logout_admin">
                            <i class="fa fa-sign-out"></i>
                            <p>Logout</p>
                        </a>
                    </li>
                </ul>

            </div>
        </div>
    </nav>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- <div class="col-lg-4 col-md-5">
                    <div class="card card-user">
                        <div class="image">
                            <img src="<?=base_url();?>assets/img/background.jpg" alt="..."/>
                        </div>
                        <div class="content">
                            <div class="author">
                                <?php
                                    $foto = $this->session->userdata('foto');
                                    ?>
                              <img class="avatar border-white" src="<?=base_url();?>assets/user/<?= $foto;?>" alt="..."/><br>
                              <a class='btn btn-info'><i class='fa fa-camera' data-toggle='modal' data-target='#changeFoto'></i></a><br>
                              <br><h4 class="title" id="name">
                              <?php
                                    $nama = $this->session->userdata('nama_lengkap');
                                    echo $nama
                                ?>
                              </h4>
                              <h6 class="title" id="name">
                              <?php
                                    $jabatan = $this->session->userdata('jabatan');
                                    echo $jabatan
                                ?>
                              </h4>
                            </div>
                        </div>
                        <hr>
                    </div>
                </div> -->

                
                <div class="col-lg-12 col-md-7">
                    <div class="card">
                        <div class="header">
                            <h4 class="title">Profil</h4>
                        </div>
                        <div class="content">
                            <form id="updateProfile" method="post" action="<?= base_url();?>index.php/admin/updateProfil">
                                <input type="hidden" id="id_user" name="id_user" class="form-control border-input" value="<?= $this->session->userdata('id');?>">    
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group" id="nama_lengkap">
                                            <label for="">Nama Lengkap</label>
                                            <input required type="text" name="nama_lengkap" class="form-control border-input" value="<?= $this->session->userdata('nama_lengkap');?>">
                                        </div>
                                    </div>
                                </div>    
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group" id="saldo">
                                            <label for="">Saldo</label>
                                            <input required type="text" name="saldo" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control border-input" value="Rp<?= $this->session->userdata('saldo');?>,-">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group" id="username">
                                            <label for="">Username</label>
                                            <input required type="text" name="username" class="form-control border-input" value="<?= $this->session->userdata('username');?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group" id="password">
                                            <label for="">Password</label>
                                            <input required type="password" name="password" class="form-control border-input" id="myInput" value="<?= $this->session->userdata('pw');?>"><br>
                                            <input type="checkbox" onclick="myFunction()" class="">Tampilkan Password
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group" id="no_telp">
                                            <label for="">No Telp</label>
                                            <input required type="text" name="no_telp" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control border-input" value="<?= $this->session->userdata('no_telp');?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group" id="alamat">
                                            <label for="">Alamat</label>
                                            <textarea required name="alamat" id="" cols="30" rows="10" class="form-control border-input"><?= $this->session->userdata('alamat');?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group" id="id_sektor">
                                            <label for="">Sektor</label>
                                            <select name="id_sektor" required class="form-control border-input" id="">
                                                <option value="">-- Pilih Sektor --</option>
                                                <?php foreach ($sektor as $sektor) { ?>
                                                <option value="<?= $sektor['id'];?>"><?= $sektor['sektor'];?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div> -->
                                <div class="text-center">
                                    <!-- <button type="submit" id="update" class="btn btn-info btn-fill btn-wd">Perbarui Profil</button> -->
                                </div>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="changeFoto" class="modal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                         <div class="modal-header">
                            <h5 class="modal-title">Ganti Foto</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <div class="modal-body">
                        <form id="update" method="post" action="<?= base_url();?>index.php/admin/changeFotoProfil" enctype="multipart/form-data">
                        <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group" id="input_id">
                                        <input type="hidden" name="id_user" value="<?= $this->session->userdata('id');?>">
                                        <input class="form-control border-input" required type="file" name="foto">
                                    </div>
                                </div>
                            </div>  

                        <div class="clearfix"></div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-info btn-fill btn-wd">
                                    Ganti Foto
                                </button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script>
        function myFunction() {
            var x = document.getElementById("myInput");
            if (x.type === "password") {
                x.type = "text";
            } else {
                x.type = "password";
            }
        }
</script>    