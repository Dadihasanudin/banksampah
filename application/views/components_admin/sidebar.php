<?php
    $uri = $this->uri->segment(2);
?>
<div class="sidebar" data-background-color="white" data-active-color="danger">
    <!--
		Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
		Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
	-->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <center><img src="<?= base_url();?>assets/img/logo.png" style="width:120px;height:50px" alt=""></center>
                </a>
            </div>

            <ul class="nav">
            <?php
                if ($uri == 'getProfil') { ?>
                    <li class="active">
                <?php } else { ?>
                    <li>
                <?php } ?>
                    <a href="<?= base_url();?>admin/getProfil">
                        <i class="fa fa-user-circle"></i>
                        <p>Profil</p>
                    </a>
                </li>

            <?php
                if ($uri == 'getPegawai') { ?>
                    <li class="active">
                <?php } else { ?>
                    <li>
                <?php } ?>
                    <a href="<?= base_url();?>admin/getPegawai">
                        <i class="fa fa-id-badge"></i>
                        <p>Petugas</p>
                    </a>
                </li>
    
            <!-- <?php
                if ($uri == 'getBarang') { ?>
                    <li class="active">
                <?php } else { ?>
                    <li>
                <?php } ?>    
                    <a href="<?= base_url();?>admin/getBarang">
                        <i class="fa fa-cubes"></i>
                        <p>Barang</p>
                    </a>
                </li> -->

            <?php
                if ($uri == 'getSektor') { ?>
                    <li class="active">
                <?php } else { ?>
                    <li>
                <?php } ?>
                    <a href="<?= base_url();?>admin/getSektor">
                        <i class="fa fa-th-list"></i>
                        <p>Sektor</p>
                    </a>
                </li>
          
            <?php
                if ($uri == 'getJenisSampah') { ?>
                    <li class="active">
                <?php } else { ?>
                    <li>
                <?php } ?>
                    <a href="<?= base_url();?>admin/getJenisSampah">
                        <i class="fa fa-trash-o"></i>
                        <p>Jenis Sampah</p>
                    </a>
                </li>
          
            <?php
                if ($uri == 'getNasabah') { ?>
                    <li class="active">
                <?php } else { ?>
                    <li>
                <?php } ?>
                    <a href="<?= base_url();?>admin/getNasabah">
                        <i class="fa fa-users"></i>
                        <p>Nasabah</p>
                    </a>
                </li>

            <!-- <?php
                if ($uri == 'getRequest') { ?>
                    <li class="active">
                <?php } else { ?>
                    <li>
                <?php } ?>
                    <a href="<?= base_url();?>admin/getRequest">
                        <i class="fa fa-inbox"></i>
                        <p>Permintaan Panggilan</p>
                    </a>
                </li> -->

            <?php
                if ($uri == 'getTransaksiSampah') { ?>
                    <li class="active">
                <?php } else { ?>
                    <li>
                <?php } ?>
                    <a href="<?= base_url();?>admin/getTransaksiSampah">
                        <i class="fa fa-trash"></i>
                        <p>Daftar Transaksi</p>
                    </a>
                </li>

            <!-- <?php
                if ($uri == 'getTransaksiBarang') { ?>
                    <li class="active">
                <?php } else { ?>
                    <li>
                <?php } ?>
                    <a href="<?= base_url();?>admin/getTransaksiBarang">
                        <i class="fa fa-shopping-basket"></i>
                        <p>Transaksi Barang</p>
                    </a>
                </li> -->
                
            </ul>
    	</div>
    </div>