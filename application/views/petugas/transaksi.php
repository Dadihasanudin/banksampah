<div class="main-panel">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar bar1"></span>
                    <span class="icon-bar bar2"></span>
                    <span class="icon-bar bar3"></span>
                </button>
                <a class="navbar-brand">Tambah</a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="<?=base_url();?>login/logout_admin">
                            <i class="fa fa-sign-out"></i>
                            <p>Logout</p>
                        </a>
                    </li>
                </ul>

            </div>
        </div>
    </nav>

    <div class="content">
        <div class="container-fluid">
                
                <div class="col-lg-12 col-md-7">
                    <div class="card">
                        <div class="header">
                            <h4 class="title">Transaksi</h4>
                        </div>
                        <div class="content">
                            <form method="post" action="<?php echo base_url('Admin/insertJenisSampah') ?>">
                                <input type="hidden"name="id_user" class="form-control border-input">   
                                <br>                
                                <br>                
                                <br>                
                                <br>                
                                <br>               
                                <br>                
                                <div class="text-center">
                                    <a href='<?= base_url();?>petugas/tabung/' class='btn btn-info btn-fill btn-wd'>Menabung</a>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                                    <a href='<?= base_url();?>petugas/tarik/' class='btn btn-info btn-fill btn-wd'>Tarik Uang</a>
                                </div>
                                <br>               
                                <br>                
                                <br>                
                                <br>                
                                <br>                
                                <br>                
                                <br>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>