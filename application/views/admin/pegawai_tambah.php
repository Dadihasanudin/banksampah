<div class="main-panel">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar bar1"></span>
                    <span class="icon-bar bar2"></span>
                    <span class="icon-bar bar3"></span>
                </button>
                <a class="navbar-brand">Tambah</a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="<?=base_url();?>login/logout_admin">
                            <i class="fa fa-sign-out"></i>
                            <p>Logout</p>
                        </a>
                    </li>
                </ul>

            </div>
        </div>
    </nav>

    <div class="content">
        <div class="container-fluid">
                
                <div class="col-lg-12 col-md-7">
                    <div class="card">
                        <div class="header">
                            <h4 class="title">Tambah Petugas</h4>
                        </div>
                        <div class="content">
                            <form method="post" action="<?php echo base_url('Admin/insertPegawai') ?>">
                                <input type="hidden"name="id_user" class="form-control border-input">   
                                <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Foto</label>
                                        <input type="text" required class="form-control border-input" name="foto">
                                    </div>
                                </div>
                            </div>   
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group" id="nama_lengkap">
                                            <label for="">Nama Lengkap</label>
                                            <input required type="text" name="nama_lengkap" class="form-control border-input">
                                        </div>
                                    </div>
                                </div>    
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group" id="username">
                                            <label for="">Username</label>
                                            <input required type="text" name="username" class="form-control border-input">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group" id="password">
                                            <label for="">Password</label>
                                            <input required type="password" name="password" class="form-control border-input" id="myInput"><br>
                                            <input type="checkbox" onclick="myFunction()" class="">Tampilkan Password
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group" id="no_telp">
                                            <label for="">No Telp</label>
                                            <input required type="text" name="no_telp" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control border-input">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group" id="alamat">
                                            <label for="">Alamat</label>
                                            <textarea required name="alamat" id="" cols="30" rows="10" class="form-control border-input"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group" id="id_sektor">
                                            <label for="">Sektor</label>
                                            <select name="id_sektor" required class="form-control border-input" id="">
                                                <option value="">-- Pilih Sektor --</option>
                                                <?php foreach ($sektor as $sektor) { ?>
                                                <option value="<?= $sektor['id'];?>"><?= $sektor['sektor'];?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button type="submit" id="update" class="btn btn-info btn-fill btn-wd">Tambah Petugas</button>
                                    <a href='<?= base_url();?>admin/getPegawai/' class='btn btn-info btn-fill btn-wd'>Kembali</a>
                                </div>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    

<script>
        function myFunction() {
            var x = document.getElementById("myInput");
            if (x.type === "password") {
                x.type = "text";
            } else {
                x.type = "password";
            }
        }
</script>    