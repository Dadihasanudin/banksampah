<div class="main-panel">
		<nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand">Nasabah</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
						<li>
                            <a href="<?= base_url();?>login/logout_admin">
								<i class="fa fa-sign-out"></i>
								<p>Logout</p>
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <!-- <div class="header">
                                <h4 class="title"><button class="btn btn-success btn-fill" type="button" id="btn_input" data-toggle="modal" data-target="#addNasabah"><i class="fa fa-plus"></i></button></h4>
                            </div> -->
                            <div class="content table-responsive table-full-width">
                                <table id="example" class="display" style="width:900" cellpading="10" cellspacing="1">
                                    <thead>
                                        <tr>
                                            <th width="10"><center><b>No</b></center></th>
                                            <th width="20"><center><b>Nama Lengkap</b></center></th>
                                            <th width="20"><center><b>No Rekening</b></center></th>
                                            <th width="20"><center><b>Username</b></center></th>
                                            <th width="20"><center><b>No Telp</b></center></th>
                                            <th width="100"><center><b>Sektor</b></center></th>
                                            <th width="100"><center><b>Alamat</b></center></th>
                                            <!-- <th width="20"><center><b>Saldo</b></center></th> -->
                                            <!-- <th width="10"><center><b>Point</b></center></th> -->
                                            <th width="20"><center><b>Status</b></center></th>
                                            <!-- <th width="20"><center><b>Aksi</b></center></th> -->
                                        </tr>
                                    </thead>
                                    <tbody id="table-row">
                                        <?php
                                        $no = 0;
                                        foreach ($nasabah as $nasabah) {
                                            $no++; ?>
                                        <tr>
                                            <td width="10"><center><b><?= $no; ?></b></center></td>
                                            <td width="20"><center><b><?= $nasabah['nama_lengkap']; ?></b></center></td>
                                            <td width="20"><center><b><?= $nasabah['no_rekening']; ?></b></center></td>
                                            <td width="20"><center><b><?= $nasabah['username']; ?></b></center></td>
                                            <td width="20"><center><b><?= $nasabah['no_telp']; ?></b></center></td>
                                            <td width="100"><center><b><?= $nasabah['sektor']; ?></b></center></td>
                                            <td width="100"><center><b><?= $nasabah['alamat']; ?></b></center></td>
                                            <!-- <td width="20"><center><b>Rp.<?= number_format($nasabah['saldo'],2,',','.'); ?></b></center></td> -->
                                            <!-- <td width="20"><center><b><?= $nasabah['point']; ?></b></center></td> -->
                                            <td width="20"><center><b><?= $nasabah['status']; ?></b></center></td>
                                            <!-- <td width="20"><center> -->
                                            <!-- <a href='<?= base_url();?>admin/deleteNasabah/<?= $nasabah['id_nasabah'];?>' class='btn btn-danger'><i class='fa fa-trash'></i></a> -->
                                            <!-- <a id='btn_edit' data-id_edit='<?= $nasabah['id_nasabah']; ?>' data-nama_lengkap_edit='<?= $nasabah['nama_lengkap']; ?>' class='btn btn-info' data-toggle='modal' data-target='#editNasabah'><i class='fa fa-pencil'></i></a> -->
                                            <?php if ($nasabah['status'] == 'aktif'){?>
                                            <!-- <a id='btn_tukar' data-id_edit='<?= $nasabah['id_nasabah']; ?>' data-nama_lengkap_edit='<?= $nasabah['nama_lengkap']; ?>' class='btn btn-warning' data-toggle='modal' data-target='#tukarUang'><i class='fa fa-money'></i></a> -->
                                            <?php } ?>
                                            </center></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="addNasabah" class="modal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                         <div class="modal-header">
                            <h5 class="modal-title">Tambah Nasabah</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <div class="modal-body">
                        <form id="add" action="<?= base_url();?>admin/insertNasabah" method="post" enctype="multipart/form-data">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nama Nasabah</label>
                                        <input type="text" required id="nama_lengkap" class="form-control border-input" placeholder="Nama Lengkap" value="" name="nama_lengkap">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Username</label>
                                        <input type="text" required id="username" class="form-control border-input" placeholder="Username" value="" name="username">
                                    </div>
                                </div>
                            </div>

        
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>No Telp</label>
                                        <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required id="no_telp" class="form-control border-input" placeholder="No Telp" value="" name="no_telp">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group" id="input_sektor">
                                        <label>Sektor</label>
                                        <select name="id_sektor" required class="form-control border-input" id="sektor_edit">
                                            <option value="">-- Pilih Sektor --</option>
                                            <?php foreach ($sektor as $sektor) { ?>
                                            <option value="<?= $sektor['id'];?>"><?= $sektor['sektor'];?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">   
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">Alamat</label>
                                        <textarea name="alamat" id="alamat" class="form-control border-input" required cols="30" rows="10"></textarea>
                                    </div>
                                </div>    
                            </div>


                        
                            <div class="clearfix"></div>
                            <div class="modal-footer">
                                <button id="btn_add" type="submit" class="btn btn-success btn-fill btn-wd">
                                    Tambah Nasabah
                                </button>
                                <button type="submit" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div id="tukarUang" class="modal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                         <div class="modal-header">
                            <h5 class="modal-title">Tukar Saldo Ke Uang</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <div class="modal-body">
                        <form id="update" method="post" action="<?= base_url();?>admin/tukarUang">
                        <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group" id="input_id">

                                    </div>
                                </div>
                            </div>  

                        <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" id="input_nama_lengkap">
                                        
                                    </div>
                                </div>

                                 <div class="col-md-6">
                                    <div class="form-group" id="input_saldo">
                                        
                                    </div>
                                </div>
                            </div> 

                        <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group" id="input_nominal">
                                        
                                    </div>
                                </div>
                            </div>      
 

                            <div class="clearfix"></div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-info btn-fill btn-wd">
                                    Tukar Ke Uang
                                </button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                    </div>
                </div>
            </div>
        </div>

            <div id="editNasabah" class="modal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                         <div class="modal-header">
                            <h5 class="modal-title">Edit Nasabah</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <div class="modal-body">
                        <form id="update" method="post" action="<?= base_url();?>admin/updateNasabah">
                        <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group" id="input-id">

                                    </div>
                                </div>
                            </div>  

                        <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" id="input-nama-lengkap">
                                        
                                    </div>
                                </div>

                                 <div class="col-md-6">
                                    <div class="form-group" id="input_username">
                                        
                                    </div>
                                </div>
                            </div>  
 
                               
   
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" id="input_no_telp">
                                        
                                    </div>
                                </div>

                            <div class="col-md-6">
                                    <div class="form-group" id="input_jabatan">
                                        <label>Status</label>
                                        <select name="status_edit" required class="form-control border-input" id="jabatan_edit">
                                            <option value="">-- Pilih Status --</option>
                                            <option value="aktif">Aktif</option>
                                            <option value="tidak_aktif">Tidak Aktif</option>
                                        </select>
                                    </div>
                                </div>
                            </div>    
                            
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group" id="input_alamat">
                                        
                                    </div>
                                </div>
                            </div>    

                            <div class="clearfix"></div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-info btn-fill btn-wd">
                                    Edit Nasabah
                                </button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                    </div>
                </div>
            </div>
        </div>

        <hr>
    </div>
</div>
<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
    'copyHtml5', 'excelHtml5', 'pdfHtml5', 'csvHtml5'
  ]
    } );

    $(document).on('click', '#btn_edit' ,function(){
                var id = $(this).data("id_edit");
                console.log(id)
                $.get("http://localhost/banksampah/admin/showNasabah/" + id, function(data, status){
                console.log(status)
                data = JSON.parse(data);
                console.log(data)
                if (status) {
                    data.forEach(element => {
                    $('#input-id').html("")
                        $('#input-id').append("<input type='hidden' required id='id_edit' class='form-control border-input' name='id_edit' value="+element.id+">");
                    $('#input-nama-lengkap').html("")
                        $('#input-nama-lengkap').append("<label>Nama Lengkap Nasabah</label><input disabled type='text' required id='nama_lengkap_edit' class='form-control border-input' name='nama_lengkap_edit' value='"+element.nama_lengkap+"'>");
                    $('#input_username').html("")
                        $('#input_username').append("<label>Username</label><input disabled type='text' required id='username_edit' class='form-control border-input' name='username_edit' value="+element.username+">");
                    $('#input_no_telp').html("")
                        $('#input_no_telp').append("<label>No Telp</label><input disabled type='text' required id='no_telp_edit' class='form-control border-input' name='no_telp_edit' value="+element.no_telp+">");
                    $('#input_alamat').html("")
                        $('#input_alamat').append("<label for=''>Alamat</label><textarea disabled name='alamat_edit' id='alamat_edit' class='form-control border-input' required cols='30' rows='10'>"+element.alamat+"</textarea>");
                    });
                }
                else {
                    console.log('data failed')
                    }
                });
            });  

    $(document).on('click', '#btn_tukar' ,function(){
                var id = $(this).data("id_edit");
                console.log(id)
                $.get("http://localhost/banksampah/admin/showNasabah/" + id, function(data, status){
                console.log(status)
                data = JSON.parse(data);
                console.log(data)
                if (status) {
                    data.forEach(element => {
                    $('#input_id').html("")
                        $('#input_id').append("<input type='hidden' required id='id_edit' class='form-control border-input' name='id_edit' value="+element.id+">");
                    $('#input_nama_lengkap').html("")
                        $('#input_nama_lengkap').append("<label>Nama Lengkap Nasabah</label><input disabled type='text' required id='nama_lengkap_edit' class='form-control border-input' name='nama_lengkap_edit' value='"+element.nama_lengkap+"'>");
                    $('#input_saldo').html("")
                        $('#input_saldo').append("<label>Saldo</label><input type='text' required id='saldo_edit' class='form-control border-input' name='saldo_edit' value="+element.saldo+">");
                    $('#input_nominal').html("")
                        $('#input_nominal').append("<label>Nominal</label><input type='number' required id='nominal_edit' class='form-control border-input' name='nominal_edit' max='"+element.saldo+"'>");
                    });
                }
                else {
                    console.log('data failed')
                    }
                });
            });  

} );
</script>