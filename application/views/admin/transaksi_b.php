<div class="main-panel">
		<nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand">Transaksi Barang</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
						<li>
                            <a href="<?= base_url();?>login/logout_admin">
								<i class="fa fa-sign-out"></i>
								<p>Logout</p>
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="content table-responsive table-full-width">
                                <table id="example" class="display nowrap" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th width="10"><center><b>No</b></center></th>
                                            <th width="20"><center><b>Nama Nasabah</b></center></th>
                                            <th width="20"><center><b>Tanggal</b></center></th>
                                            <th width="20"><center><b>Total Harga</b></center></th>
                                            <th width="20"><center><b>Status Pembayaran</b></center></th>
                                            <th width="20"><center><b>Status Pengantaran</b></center></th>
                                            <th width="20"><center><b>Aksi</b></center></th>
                                        </tr>
                                    </thead>
                                    <tbody id="table-row">
                                        <?php
                                        $no = 0;
                                        foreach ($transaksi as $transaksi) {
                                            $no++; ?>
                                        <tr>
                                            <td width="10"><center><b><?= $no; ?></b></center></td>
                                            <td width="20"><center><b><?= $transaksi['nama_lengkap']; ?></b></center></td>
                                            <td width="20"><center><b><?= $transaksi['tanggal']; ?></b></center></td>
                                            <td width="20"><center><b>Rp.<?= number_format($transaksi['total_biaya'],2,',','.'); ?></b></center></td>
                                            <td width="20"><center><b><?= $transaksi['status_pembayaran']; ?></b></center></td>
                                            <td width="20"><center><b><?= $transaksi['status_pengantaran']; ?></b></center></td>
                                            <td width="20"><center>
                                            <a href='<?= base_url();?>admin/deleteTransaksiBarang/<?= $transaksi['id_transaksi_barang'];?>' class='btn btn-danger'><i class='fa fa-trash'></i></a>
                                            <a id='btn_edit' data-id_edit='<?= $transaksi['id_transaksi_barang']; ?>' data-nama='<?= $transaksi['nama_lengkap']; ?>' data-tanggal='<?= $transaksi['tanggal']; ?>' class='btn btn-info' data-toggle='modal' data-target='#detailTransaksiBarang'><i class='fa fa-pencil'></i></a>
                                            </center></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            
            <div id="detailTransaksiBarang" class="modal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                         <div class="modal-header">
                            <h5 class="modal-title">Detail Transaksi Barang</h5>
                            <h6 id="list_nama" class="modal-title">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="content table-responsive ">
                                <table id="example" class="display nowrap" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th width="20"><center><b>Nama Barang</b></center></th>
                                            <th width="20"><center><b>Jumlah</b></center></th>
                                            <th width="20"><center><b>Total</b></center></th>
                                        </tr>
                                    </thead>
                                    <tbody id="table-row">
                                        <tr>
                                            <td id="list_nama_barang" width="20">
                                            <td id="list_jumlah" width="20">
                                            <td id="list_total_harga" width="20">
                                            </tr>
                                    </tbody>
                                </table>
                            </div>
                </div>
            </div>
        </div>
    </div>

<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
    'copyHtml5', 'excelHtml5', 'pdfHtml5', 'csvHtml5'
  ]
    } );

    $(document).on('click', '#btn_edit' ,function(){
                var id = $(this).data("id_edit");
                var nama = $(this).data("nama");
                var tanggal = $(this).data("tanggal");
                console.log(id)
                $.get("http://localhost/banksampah/admin/showTransaksiBarang/" + id, function(data, status){
                console.log(status)
                data = JSON.parse(data);
                console.log(data)
                if (status) {
                    $('#list_nama').html("")
                        $('#list_nama').append("Atas Nama<b> "+nama+"</b>, pada tanggal <b>"+tanggal+"</b></h6>");
                    $('#list_nama_barang').html("")
                    $('#list_jumlah').html("")
                    $('#list_total_harga').html("")
                    data.forEach(element => {
                        $('#list_nama_barang').append("<center><b>"+element.nama_barang+"</b></center></td>");
                        $('#list_jumlah').append("<center><b>"+element.jumlah+"</b></center></td>");
                        $('#list_total_harga').append("<center><b>"+element.total_harga+"</b></center></td>");
                    });
                }
                else {
                    console.log('data failed')
                    }
                });
            });  

} );
</script>