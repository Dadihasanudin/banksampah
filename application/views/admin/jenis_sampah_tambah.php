<div class="main-panel">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar bar1"></span>
                    <span class="icon-bar bar2"></span>
                    <span class="icon-bar bar3"></span>
                </button>
                <a class="navbar-brand">Tambah</a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="<?=base_url();?>login/logout_admin">
                            <i class="fa fa-sign-out"></i>
                            <p>Logout</p>
                        </a>
                    </li>
                </ul>

            </div>
        </div>
    </nav>

    <div class="content">
        <div class="container-fluid">
                
                <div class="col-lg-12 col-md-7">
                    <div class="card">
                        <div class="header">
                            <h4 class="title">Tambah Jenis Sampah</h4>
                        </div>
                        <div class="content">
                            <form method="post" action="<?php echo base_url('Admin/insertJenisSampah') ?>">
                                <input type="hidden"name="id_user" class="form-control border-input">   
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group" id="jenis_sampah">
                                            <label for="">Jenis Sampah</label>
                                            <input required type="text" name="jenis_sampah" class="form-control border-input">
                                        </div>
                                    </div>
                                </div>    
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group" id="harga">
                                            <label for="">Harga</label>
                                            <input required type="text" name="harga" class="form-control border-input">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group" id="satuan">
                                            <label for="">Satuan</label>
                                            <input required type="text" name="satuan" class="form-control border-input">
                                        </div>
                                    </div>
                                </div>                                
                                <div class="text-center">
                                    <button type="submit" id="update" class="btn btn-info btn-fill btn-wd">Tambah Jenis Sampah</button>
                                    <a href='<?= base_url();?>admin/getJenisSampah/' class='btn btn-info btn-fill btn-wd'>Kembali</a>
                                </div>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    

<script>
        function myFunction() {
            var x = document.getElementById("myInput");
            if (x.type === "password") {
                x.type = "text";
            } else {
                x.type = "password";
            }
        }
</script>    