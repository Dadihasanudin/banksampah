<div class="main-panel">
		<nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand">Jenis Sampah</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
						<li>
                            <a href="<?= base_url();?>login/logout_admin">
								<i class="fa fa-sign-out"></i>
								<p>Logout</p>
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title"><a class="btn btn-success btn-fill" type="button"  href="<?php echo base_url('Admin/TambahSampah') ?>"><i class="fa fa-plus"></i></a></h4>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table id="example" class="display nowrap" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th width="10"><center><b>No</b></center></th>
                                            <th width="20"><center><b>Jenis Sampah</b></center></th>
                                            <th width="20"><center><b>Harga</b></center></th>
                                            <th width="20"><center><b>Satuan</b></center></th>
                                            <th width="20"><center><b>Aksi</b></center></th>
                                        </tr>
                                    </thead>
                                    <tbody id="table-row">
                                        <?php
                                        $no = 0;
                                        foreach ($jenis as $jenis) {
                                            $no++; ?>
                                        <tr>
                                            <td width="10"><center><b><?= $no; ?></b></center></td>
                                            <td width="20"><center><b><?= $jenis['jenis_sampah']; ?></b></center></td>
                                            <td width="20"><center><b>Rp.<?= number_format($jenis['harga'],2,',','.'); ?></b></center></td>
                                            <td width="20"><center><b><?= $jenis['satuan']; ?></b></center></td>
                                            <td width="20"><center>
                                            <a href='<?= base_url();?>admin/deleteJenisSampah/<?= $jenis['id'];?>' class='btn btn-danger'><i class='fa fa-trash'></i></a>
                                            <a href='<?= base_url();?>admin/editJenisSampah/<?= $jenis['id'];?>' class='btn btn-info'><i class='fa fa-pencil'></i></a>
                                            <!-- <a>lalala</a> -->
                                            <!-- <a href="<?php echo base_url('Admin//' ) ?>" class="btn btn-info" ><i class="fa fa-pencil"></i></a> -->
                                            </center></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- <div id="addJenisSampah" class="modal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                         <div class="modal-header">
                            <h5 class="modal-title">Tambah Jenis Sampah</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <div class="modal-body">
                        <form id="add" action="<?= base_url();?>admin/insertJenisSampah" method="post" enctype="multipart/form-data">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Jenis Sampah</label>
                                        <input type="text" required id="jenis_sampah" class="form-control border-input" placeholder="Jenis Sampah" value="" name="jenis_sampah">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Harga</label>
                                        <input type="text" required onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="harga" class="form-control border-input" placeholder="Harga Sampah" value="" name="harga">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Satuan</label>
                                        <input type="text" required id="satuan" class="form-control border-input" placeholder="Satuan" value="" name="satuan">
                                    </div>
                                </div>
                            </div>
                        
                            <div class="clearfix"></div>
                                <div class="modal-footer">
                                    <button id="btn_add" type="submit" class="btn btn-success btn-fill btn-wd">
                                        Tambah Jenis Sampah
                                    </button>
                                    <button type="submit" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            <div id="editJenisSampah" class="modal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                         <div class="modal-header">
                            <h5 class="modal-title">Edit Jenis Sampah</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <div class="modal-body">
                        <form id="update" method="post" action="<?= base_url();?>admin/updateJenisSampah">
                        <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group" id="input_id">

                                    </div>
                                </div>
                            </div>  

                        <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group" id="input_jenis_sampah">
                                        
                                    </div>
                                </div>
                        </div>

                        <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group" id="input_harga">
                                        
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group" id="input_satuan">
                                        
                                    </div>
                                </div>
                            </div>  

                        <div class="clearfix"></div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-info btn-fill btn-wd">
                                    Edit Jenis Sampah
                                </button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div> -->

        <hr>
    </div>
</div>

<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
    'copyHtml5', 'excelHtml5', 'pdfHtml5', 'csvHtml5'
  ]
    } );

    $(document).on('click', '#btn_edit' ,function(){
                var id = $(this).data("id_edit");
                console.log(id)
                $.get("http://localhost/banksampah/admin/showJenisSampah/" + id, function(data, status){
                console.log(status)
                data = JSON.parse(data);
                console.log(data)
                if (status) {
                    data.forEach(element => {
                    $('#input_id').html("")
                        $('#input_id').append("<input type='hidden' required id='id_edit' class='form-control border-input' name='id_edit' value="+element.id+">");
                    $('#input_jenis_sampah').html("")
                        $('#input_jenis_sampah').append("<label>Jenis Sampah</label><input type='text' required id='jenis_sampah_edit' class='form-control border-input' name='jenis_sampah_edit' value='"+element.jenis_sampah+"'>");
                    $('#input_harga').html("")
                        $('#input_harga').append("<label>Harga</label><input onkeypress='return event.charCode >= 48 && event.charCode <= 57' type='text' required id='harga_edit' class='form-control border-input' name='harga_edit' value="+element.harga+">");
                    $('#input_satuan').html("")
                        $('#input_satuan').append("<label>Satuan</label><input type='text' required id='satuan_edit' class='form-control border-input' name='satuan_edit' value="+element.satuan+">");
                    });
                }
                else {
                    console.log('data failed')
                    }
                });
            });  

} );
</script>