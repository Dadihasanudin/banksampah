<div class="main-panel">
		<nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand">Petugas</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
						<li>
                            <a href="<?= base_url();?>login/logout_admin">
								<i class="fa fa-sign-out"></i>
								<p>Logout</p>
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title"><a href='<?= base_url();?>admin/tambahPegawai/' class="btn btn-success btn-fill" type="button" id="btn_input"><i class="fa fa-plus"></i></a ></h4>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table id="example" class="display nowrap" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th width="10"><center><b>No</b></center></th>
                                            <th width="20"><center><b>Nama Lengkap</b></center></th>
                                            <th width="20"><center><b>Username</b></center></th>
                                            <th width="20"><center><b>No Telp</b></center></th>
                                            <!-- <th width="20"><center><b>Alamat</b></center></th> -->
                                            <th width="20"><center><b>Jabatan</b></center></th>
                                            <th width="20"><center><b>Status</b></center></th>
                                            <th width="20"><center><b>Sektor</b></center></th>
                                            <th width="20"><center><b>Aksi</b></center></th>
                                        </tr>
                                    </thead>
                                    <tbody id="table-row">
                                        <?php
                                        $no = 0;
                                        foreach ($pegawai as $pegawai) {
                                            $no++; ?>
                                            <?php if ($pegawai['jabatan'] == 'admin'){?>

                                        <tr style="background-color:lightgreen">
                                            <?php } else{?>
                                        <tr>
                                            <?php } ?>
                                            <td width="10"><center><b><?= $no; ?></b></center></td>
                                            <td width="20"><center><b><?= $pegawai['nama_lengkap']; ?></b></center></td>
                                            <td width="20"><center><b><?= $pegawai['username']; ?></b></center></td>
                                            <td width="20"><center><b><?= $pegawai['no_telp']; ?></b></center></td>
                                            <!-- <td width="20"><center><b><?= $pegawai['alamat']; ?></b></center></td> -->
                                            <td width="20"><center><b><?= $pegawai['jabatan']; ?></b></center></td>
                                            <td width="20"><center><b><?= $pegawai['status']; ?></b></center></td>
                                            <td width="20"><center><b><?= $pegawai['sektor']; ?></b></center></td>
                                            <td width="20"><center>
                                            <a href='<?= base_url();?>admin/deletePegawai/<?= $pegawai['id'];?>' class='btn btn-danger'><i class='fa fa-trash'></i></a>
                                            <a href='<?= base_url();?>admin/editPegawai/<?= $pegawai['id']; ?>' class='btn btn-info'><i class='fa fa-pencil'></i></a>
                                            </center></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="addPegawai" class="modal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                         <div class="modal-header">
                            <h5 class="modal-title">Tambah Pegawai</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <div class="modal-body">
                        <form id="add" action="<?= base_url();?>admin/insertPegawai" method="post" enctype="multipart/form-data">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nama Pegawai</label>
                                        <input type="text" required id="nama_lengkap" class="form-control border-input" placeholder="Nama Lengkap" value="" name="nama_lengkap">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Username</label>
                                        <input type="text" required id="username" class="form-control border-input" placeholder="Username" value="" name="username">
                                    </div>
                                </div>
                            </div>

        
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>No Telp</label>
                                        <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required id="no_telp" class="form-control border-input" placeholder="No Telp" value="" name="no_telp">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Jabatan</label>
                                        <select name="jabatan" required class="form-control border-input" id="jabatan">
                                            <option value="">-- Pilih Jabatan --</option>
                                            <option value="admin">Admin</option>
                                            <option value="petugas">Petugas</option>
                                        </select>
                                    </div>
                                </div>
                            </div>    
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" id="input_sektor">
                                        <label>Sektor</label>
                                        <select name="id_sektor" required class="form-control border-input" id="sektor_edit">
                                            <option value="">-- Pilih Sektor --</option>
                                            <?php foreach ($sektor as $sektor) { ?>
                                            <option value="<?= $sektor['id'];?>"><?= $sektor['sektor'];?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Foto</label>
                                        <input type="file" required id="foto" class="form-control border-input" placeholder="No Telp" value="" name="foto">
                                    </div>
                                </div>
                            </div>    

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">Alamat</label>
                                        <textarea name="alamat" id="alamat" class="form-control border-input" required cols="30" rows="10"></textarea>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="clearfix"></div>
                            <div class="modal-footer">
                                <button id="btn_add" type="submit" class="btn btn-success btn-fill btn-wd">
                                    Tambah Pegawai
                                </button>
                                <button type="submit" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <hr>
    </div>
</div>
<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
    'copyHtml5', 'excelHtml5', 'pdfHtml5', 'csvHtml5'
  ]
    } );
} );
</script>