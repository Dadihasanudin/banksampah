<?php
    $uri = $this->uri->segment(2);
?>
<div class="sidebar" data-background-color="white" data-active-color="danger">
    <!--
		Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
		Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
	-->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <center><img src="<?= base_url();?>assets/img/logo.png" style="width:120px;height:50px" alt=""></center>
                </a>
            </div>

            <ul class="nav">
            <?php
                if ($uri == 'getProfil') { ?>
                    <li class="active">
                <?php } else { ?>
                    <li>
                <?php } ?>
                    <a href="<?= base_url();?>petugas/getProfil">
                        <i class="fa fa-user-circle"></i>
                        <p>Profil</p>
                    </a>
                </li>
            
            <?php
                if ($uri == 'getNasabah') { ?>
                    <li class="active">
                <?php } else { ?>
                    <li>
                <?php } ?>
                    <a href="<?= base_url();?>petugas/getNasabah">
                        <i class="fa fa-users"></i>
                        <p>Nasabah</p>
                    </a>
                </li>

            <?php
                if ($uri == 'getRequest') { ?>
                    <li class="active">
                <?php } else { ?>
                    <li>
                <?php } ?>
                    <a href="<?= base_url();?>petugas/transaksi ">
                        <i class="fa fa-inbox"></i>
                        <p>Transaksi</p>
                    </a>
                </li>

            <?php
                if ($uri == 'getTransaksiSampah') { ?>
                    <li class="active">
                <?php } else { ?>
                    <li>
                <?php } ?>
                    <a href="<?= base_url();?>petugas/getTransaksiSampah">
                        <i class="fa fa-trash"></i>
                        <p>Daftar Transaksi</p>
                    </a>
                </li>


            <!-- <?php
                if ($uri == 'getTransaksiSampah') { ?>
                    <li class="active">
                <?php } else { ?>
                    <li>
                <?php } ?>
                    <a href="<?= base_url();?>petugas/getTransaksiSampah">
                        <i class="fa fa-trash"></i>
                        <p>Transaksi Sampah</p>
                    </a>
                </li> -->
                
            </ul>
    	</div>
    </div>