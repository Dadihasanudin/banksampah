-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 09 Sep 2019 pada 03.59
-- Versi Server: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_bank_sampah`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_barang`
--

CREATE TABLE `tb_barang` (
  `id` int(11) NOT NULL,
  `nama_barang` varchar(50) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `deskripsi` text NOT NULL,
  `harga` int(11) NOT NULL,
  `stok` int(11) NOT NULL,
  `foto` text NOT NULL,
  `jenis` enum('regular','eksklusif') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_barang`
--

INSERT INTO `tb_barang` (`id`, `nama_barang`, `id_kategori`, `deskripsi`, `harga`, `stok`, `foto`, `jenis`) VALUES
(1, 'Seragam PDL', 2, 'Seragam luar', 50000, 15, 'pdl.jpg', 'regular'),
(2, 'Ayam Goreng Haneut', 1, 'Ayam Goreng Hangat Mantull kuk', 12001, 54, '1845312.png', 'regular');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_jenis_sampah`
--

CREATE TABLE `tb_jenis_sampah` (
  `id` int(11) NOT NULL,
  `jenis_sampah` varchar(20) NOT NULL,
  `harga` int(11) NOT NULL,
  `satuan` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_jenis_sampah`
--

INSERT INTO `tb_jenis_sampah` (`id`, `jenis_sampah`, `harga`, `satuan`) VALUES
(1, 'botol plastik ', 5000, 'kg'),
(2, 'kardus', 1000, 'kg'),
(3, 'lala', 1000, 'KG'),
(4, 'lala', 1000, 'KG'),
(5, 'lala', 1000, 'kg'),
(6, 'lala', 1000, 'kg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kategori`
--

CREATE TABLE `tb_kategori` (
  `id` int(11) NOT NULL,
  `nama_kategori` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_kategori`
--

INSERT INTO `tb_kategori` (`id`, `nama_kategori`) VALUES
(1, 'Makanan & Minuman'),
(2, 'Pakaian');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_nasabah`
--

CREATE TABLE `tb_nasabah` (
  `id` int(11) NOT NULL,
  `nama_lengkap` varchar(50) NOT NULL,
  `no_rekening` varchar(13) NOT NULL,
  `alamat` text NOT NULL,
  `id_sektor` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `no_telp` varchar(12) NOT NULL,
  `saldo` int(11) NOT NULL,
  `status` enum('aktif','tidak_aktif') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_nasabah`
--

INSERT INTO `tb_nasabah` (`id`, `nama_lengkap`, `no_rekening`, `alamat`, `id_sektor`, `username`, `password`, `no_telp`, `saldo`, `status`) VALUES
(1, 'Visca', '2147480000012', 'polposa', 1, 'viscanasabah', '19bdc9e76ce067fa741d0c09267ecf3d', '081111111234', 168000, 'aktif'),
(2, 'eko', '606472340676', 'suk', 3, 'ekonasabah', '11ccf8f82540766c5cb01a628244246e', '081111111234', 35000, 'aktif'),
(3, 'Joni anwar', '39424857880', 'Jl.mekar wangi No.5', 7, 'joni', '1281d0ac7a74eb91550ff52a02862cda', '081348532159', 95000, 'aktif'),
(4, 'ryan', '808400918627', 'Jl.resdytrd no.42', 1, 'ryan', '10c7ccc7a4f0aff03c915c485565b9da', '084624156446', 0, 'aktif'),
(5, 'haci', '753331793486', 'JL.c No.5', 12, 'haci', '8ab10fdece99a775ab485cb2a50fa721', '084613579642', 0, 'aktif'),
(7, 'Hasan', '383660882643', 'Jl.merak no.6', 5, 'Hasan', '2408ebcdb5b5b9b86dce68bbe89868f3', '081236475894', 0, 'aktif'),
(8, 'veli', '91470494691', 'Jl.mawar No.54', 5, 'veli', '4799d7258653f1c1ad6f6b6718fb9af5', '082341567432', 0, 'aktif'),
(9, 'haci', '37425993938', 'Jl.mawar No.79', 7, 'haci', '8ab10fdece99a775ab485cb2a50fa721', '0757', 0, 'aktif'),
(10, 'a', '769562449928', 'fghbnjk', 5, 'a', '0cc175b9c0f1b6a831c399e269772661', '56789', 0, 'aktif'),
(11, 'ytfdluyt', '797452829400', 'fysdxtrdcfuytf', 7, 'fuyhytgc', '4bcf9802d670fe1714539e0defbddddd', '79867', 0, 'aktif'),
(12, 'fhdxytf', '385570453735', 'ytdcyguiljkhjkhfy', 6, 'tyddcdxvuyifytuyhyg', 'd528d20d980e0e80b35e3252ef5a3771', '876987096789', 0, 'aktif'),
(13, 'anju', '71188911125', 'Jl.hsavfdh', 1, 'anju', '9abfae448bc00ea3214033a3086e6539', '087538459826', 0, 'aktif'),
(14, 'baba', '594163528932', 'baba', 1, 'baba', '21661093e56e24cd60b10092005c4ac7', '0909', 0, 'aktif'),
(15, 'baba', '4148463805', 'baba', 1, 'baba', '21661093e56e24cd60b10092005c4ac7', '0909', 0, 'aktif'),
(16, 'baba', '603849326107', 'baba', 1, 'baba', '21661093e56e24cd60b10092005c4ac7', '0909', 0, 'aktif'),
(17, 'baba', '97665959917', 'baba', 1, 'baba', '21661093e56e24cd60b10092005c4ac7', '0909', 0, 'aktif'),
(18, 'baba', '713212888533', 'baba', 1, 'baba', '21661093e56e24cd60b10092005c4ac7', '0909', 0, 'aktif'),
(19, 'caca', '72866552250', 'caca', 1, 'caca', 'd2104a400c7f629a197f33bb33fe80c0', '09', 0, 'aktif'),
(20, 'caca', '81410383874', 'caca', 1, 'caca', 'd2104a400c7f629a197f33bb33fe80c0', '09', 0, 'aktif'),
(21, 'caca', '18420363717', 'caca', 1, 'caca', 'd2104a400c7f629a197f33bb33fe80c0', '09', 0, 'aktif'),
(22, 'caca', '522991596986', 'caca', 1, 'caca', 'd2104a400c7f629a197f33bb33fe80c0', '09', 0, 'aktif'),
(23, 'caca', '450195692861', 'caca', 1, 'caca', 'd2104a400c7f629a197f33bb33fe80c0', '09', 0, 'aktif'),
(24, 'tdty', '88426762059', 'jhvyu', 3, 'gfyg', 'ce7c04ff21f341b65ceb0c9eddae9da4', '9087656', 0, 'aktif'),
(25, 'dx', '23876139245', 'Jl.mawar', 2, 'dxd', '279a02b4cea61841880cd4ccb417eac1', '08765', 0, 'aktif'),
(26, 'Andi', '35645778653', 'Jl.Kenanga no.5', 3, 'andi', 'ce0e5bf55e4f71749eade7a8b95c4e46', '081737465382', 532000, 'aktif');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_request`
--

CREATE TABLE `tb_request` (
  `id` int(11) NOT NULL,
  `id_nasabah` int(11) NOT NULL,
  `id_pegawai` int(11) DEFAULT NULL,
  `jenis_request` enum('penukaran sampah','penukaran uang') NOT NULL,
  `status` enum('pending','diproses','menuju','selesai') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_request`
--

INSERT INTO `tb_request` (`id`, `id_nasabah`, `id_pegawai`, `jenis_request`, `status`) VALUES
(1, 1, 2, 'penukaran sampah', 'selesai'),
(2, 2, 4, 'penukaran sampah', 'selesai'),
(3, 2, 5, 'penukaran sampah', 'selesai'),
(4, 2, 5, 'penukaran sampah', 'selesai'),
(5, 1, 10, 'penukaran sampah', 'selesai'),
(6, 1, 10, 'penukaran sampah', 'selesai'),
(7, 5, 10, 'penukaran uang', 'pending'),
(8, 4, 10, 'penukaran sampah', 'pending'),
(9, 3, 10, 'penukaran uang', 'pending'),
(10, 7, 10, 'penukaran sampah', 'pending'),
(11, 5, 10, 'penukaran sampah', 'pending'),
(12, 8, 10, '', 'pending'),
(13, 8, 10, '', 'pending'),
(14, 5, 10, '', 'pending'),
(15, 5, 10, 'penukaran uang', 'pending'),
(16, 2, 10, 'penukaran uang', 'pending'),
(17, 26, 10, 'penukaran sampah', 'selesai');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_sektor`
--

CREATE TABLE `tb_sektor` (
  `id` int(11) NOT NULL,
  `sektor` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_sektor`
--

INSERT INTO `tb_sektor` (`id`, `sektor`) VALUES
(2, 'bumiwangi'),
(3, 'ciheulang'),
(4, 'cikoneng'),
(5, 'gunungleutik'),
(6, 'manggungharja'),
(7, 'mekar laksana'),
(8, 'mekarsari'),
(9, 'pakutandang'),
(10, 'sarimahi serangmekar'),
(11, 'Mantuan'),
(12, 'ciparay'),
(13, 'sumbersari'),
(14, 'asd');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_tarikuang`
--

CREATE TABLE `tb_tarikuang` (
  `id` int(11) NOT NULL,
  `id_nasabah` int(11) NOT NULL,
  `id_petugas` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `nominal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_tarikuang`
--

INSERT INTO `tb_tarikuang` (`id`, `id_nasabah`, `id_petugas`, `tanggal`, `nominal`) VALUES
(1, 1, 1, '2019-08-20', 5000),
(2, 2, 1, '2019-08-20', 5000),
(3, 1, 1, '2019-08-20', 5000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_transaksi_barang`
--

CREATE TABLE `tb_transaksi_barang` (
  `id_transaksi_barang` int(11) NOT NULL,
  `id_nasabah` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `total_biaya` int(11) NOT NULL,
  `status_pembayaran` enum('lunas','belum_lunas','','') NOT NULL,
  `status_pengantaran` enum('pending','diantar','selesai','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_transaksi_barang_detail`
--

CREATE TABLE `tb_transaksi_barang_detail` (
  `id_transaksi_barang_detail` int(11) NOT NULL,
  `id_transaksi_barang` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `total_harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_transaksi_sampah`
--

CREATE TABLE `tb_transaksi_sampah` (
  `id_transaksi_sampah` int(11) NOT NULL,
  `id_nasabah` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `total_harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_transaksi_sampah`
--

INSERT INTO `tb_transaksi_sampah` (`id_transaksi_sampah`, `id_nasabah`, `tanggal`, `total_harga`) VALUES
(1, 1, '2019-08-01', 25000),
(2, 2, '2019-08-02', 40000),
(3, 2, '2019-08-02', 10000),
(4, 2, '2019-08-02', 35000),
(5, 2, '2019-08-02', 35000),
(6, 1, '2019-08-02', 40000),
(7, 1, '2019-08-19', 25000),
(8, 1, '2019-08-27', 70000),
(9, 1, '2019-08-27', 38000),
(10, 26, '2019-09-08', 32000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_transaksi_sampah_detail`
--

CREATE TABLE `tb_transaksi_sampah_detail` (
  `id_transaksi_sampah_detail` int(11) NOT NULL,
  `id_transaksi_sampah` int(11) NOT NULL,
  `id_jenis_sampah` int(11) NOT NULL,
  `berat` int(11) NOT NULL,
  `harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_transaksi_sampah_detail`
--

INSERT INTO `tb_transaksi_sampah_detail` (`id_transaksi_sampah_detail`, `id_transaksi_sampah`, `id_jenis_sampah`, `berat`, `harga`) VALUES
(1, 1, 1, 4, 5000),
(2, 1, 2, 5, 1000),
(3, 2, 1, 8, 5000),
(4, 3, 1, 2, 5000),
(5, 5, 1, 7, 5000),
(6, 6, 1, 8, 5000),
(7, 7, 1, 5, 5000),
(8, 8, 1, 14, 5000),
(9, 9, 1, 7, 5000),
(10, 9, 2, 3, 1000),
(11, 10, 1, 5, 5000),
(12, 10, 2, 7, 1000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_user`
--

CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL,
  `nama_lengkap` varchar(50) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(50) NOT NULL,
  `no_telp` varchar(13) NOT NULL,
  `alamat` text NOT NULL,
  `jabatan` enum('admin','petugas') NOT NULL,
  `foto` text NOT NULL,
  `status` enum('aktif','tidak_aktif') NOT NULL,
  `id_sektor` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_user`
--

INSERT INTO `tb_user` (`id`, `nama_lengkap`, `username`, `password`, `no_telp`, `alamat`, `jabatan`, `foto`, `status`, `id_sektor`) VALUES
(1, 'Admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', '081234567899', 'Jln.Ciparay Raya Mantul', 'admin', 'tyrell.jpg', 'aktif', 13),
(2, 'Naufal Ramadhan Ku', 'naufalpetugas', '81c402ddbc018026551df68f526ea429', '081111111235', 'Jlnn.Sukarasa Leghorn58', 'petugas', '292789.jpg', 'aktif', 1),
(10, 'Dadi Hasanudin', 'dadi', '11cce4cbc871796013fa495b82ff98a6', '081734583672', 'Jl.kenangan', 'petugas', 'arryn2.jpg', 'aktif', 4),
(11, 'Ayu', 'ayu', '29c65f781a1068a41f735e1b092546de', '081323465876', 'Jl.ambon No.69', 'petugas', 'greyjoy.jpg', 'aktif', 6);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_barang`
--
ALTER TABLE `tb_barang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_kategori` (`id_kategori`);

--
-- Indexes for table `tb_jenis_sampah`
--
ALTER TABLE `tb_jenis_sampah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_kategori`
--
ALTER TABLE `tb_kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_nasabah`
--
ALTER TABLE `tb_nasabah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_request`
--
ALTER TABLE `tb_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_sektor`
--
ALTER TABLE `tb_sektor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_tarikuang`
--
ALTER TABLE `tb_tarikuang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_transaksi_barang`
--
ALTER TABLE `tb_transaksi_barang`
  ADD PRIMARY KEY (`id_transaksi_barang`);

--
-- Indexes for table `tb_transaksi_barang_detail`
--
ALTER TABLE `tb_transaksi_barang_detail`
  ADD PRIMARY KEY (`id_transaksi_barang_detail`);

--
-- Indexes for table `tb_transaksi_sampah`
--
ALTER TABLE `tb_transaksi_sampah`
  ADD PRIMARY KEY (`id_transaksi_sampah`);

--
-- Indexes for table `tb_transaksi_sampah_detail`
--
ALTER TABLE `tb_transaksi_sampah_detail`
  ADD PRIMARY KEY (`id_transaksi_sampah_detail`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_barang`
--
ALTER TABLE `tb_barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_jenis_sampah`
--
ALTER TABLE `tb_jenis_sampah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_kategori`
--
ALTER TABLE `tb_kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_nasabah`
--
ALTER TABLE `tb_nasabah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `tb_request`
--
ALTER TABLE `tb_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `tb_sektor`
--
ALTER TABLE `tb_sektor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `tb_tarikuang`
--
ALTER TABLE `tb_tarikuang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_transaksi_barang`
--
ALTER TABLE `tb_transaksi_barang`
  MODIFY `id_transaksi_barang` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_transaksi_barang_detail`
--
ALTER TABLE `tb_transaksi_barang_detail`
  MODIFY `id_transaksi_barang_detail` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_transaksi_sampah`
--
ALTER TABLE `tb_transaksi_sampah`
  MODIFY `id_transaksi_sampah` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tb_transaksi_sampah_detail`
--
ALTER TABLE `tb_transaksi_sampah_detail`
  MODIFY `id_transaksi_sampah_detail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `tb_barang`
--
ALTER TABLE `tb_barang`
  ADD CONSTRAINT `tb_barang_ibfk_1` FOREIGN KEY (`id_kategori`) REFERENCES `tb_kategori` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
